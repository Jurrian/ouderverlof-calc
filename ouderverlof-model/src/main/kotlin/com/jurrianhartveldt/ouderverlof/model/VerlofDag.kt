package com.jurrianhartveldt.ouderverlof.model

import java.time.DayOfWeek
import java.time.Duration

data class VerlofDag(
    val weekdag: DayOfWeek,
    val aantalUur: Int,
    val herhaling: Duration = Duration.ZERO,
    val offset: Duration = Duration.ZERO
)
