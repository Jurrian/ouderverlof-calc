package com.jurrianhartveldt.ouderverlof.model

import java.time.LocalDate

data class Kind(
    val naam: String,
    val geboorteDatum: LocalDate
)
