package com.jurrianhartveldt.ouderverlof.model

import java.time.LocalDate

class VerlofPeriode(val kind: Kind, val start: LocalDate, val eind: LocalDate, val verlofdagen: Set<VerlofDag>) {

}
