package com.jurrianhartveldt.ouderverlof.lib

import com.jurrianhartveldt.ouderverlof.lib.date.DateProgression
import com.jurrianhartveldt.ouderverlof.model.ContractPeriode
import com.jurrianhartveldt.ouderverlof.model.Kind
import com.jurrianhartveldt.ouderverlof.model.VerlofPeriode
import java.time.LocalDate
import java.time.LocalDate.EPOCH

/**
 * Verlof gebasseerd op 26 maal aantal uren in contract-werkweek.
 *
 * Nationale feestdagen tellen ook gewoon als verlofdagen.
 */
class VerlofCalculator {

    companion object {
        const val VERLOF_FACTOR = 26
    }

    operator fun LocalDate.rangeTo(other: LocalDate) = DateProgression(this, other)

    /**
     * Totaal Ouderverlof is berekend door 26 maal de wekelijkse contracturen per kind te nemen
     */
    fun getTotaalVerlof(contractPeriode: ContractPeriode, kinderen: List<Kind>): Int {
        return contractPeriode.weekUren * kinderen.size.coerceAtLeast(1) * VERLOF_FACTOR
    }

    /**
     * Bepaal het totaal aantal gebruikte verlofuren op een specifiek moment
     */
    fun getGebruiktVerlof(
        contractPeriodes: List<ContractPeriode>,
        verlofPeriodes: List<VerlofPeriode>
    ): Int {
        val uitersteVerlofPeriodeDatum: LocalDate = verlofPeriodes.maxByOrNull { it.eind } ?.eind ?: EPOCH

        return getGebruiktVerlofOpMoment(contractPeriodes, verlofPeriodes, uitersteVerlofPeriodeDatum)
    }

    /**
     * Bepaal het totaal aantal gebruikte verlofuren op een specifiek moment
     */
    fun getGebruiktVerlofOpMoment(
        contractPeriodes: List<ContractPeriode>,
        verlofPeriodes: List<VerlofPeriode>,
        meetMoment: LocalDate
    ): Int {
        val geldigeVerlofPeriodes = verlofPeriodes.filter { verlofPeriode ->
            contractPeriodes.filter { contractPeriode ->
                meetMoment > contractPeriode.start &&
                contractPeriode.start <= verlofPeriode.eind && contractPeriode.end >= verlofPeriode.eind
            }.any()
        }

        return getUrenUitVerlofPeriodes(geldigeVerlofPeriodes, meetMoment)
    }

    /**
     * Bereken het aantal verlofuren in een periode tot en met het meetmoment
     */
    private fun getUrenUitVerlofPeriodes(
        geldigeVerlofPeriodes: List<VerlofPeriode>,
        meetMoment: LocalDate
    ): Int {
        var gebruikteVerlofUren = 0;

        geldigeVerlofPeriodes.forEach { verlofPeriode ->
            val eindDatum = minOf(verlofPeriode.eind, meetMoment)
            for (date in verlofPeriode.start..eindDatum)
                gebruikteVerlofUren += verlofPeriode.verlofdagen
                    .filter { verlofDag -> verlofDag.weekdag == date.dayOfWeek }
                    .sumOf { it.aantalUur }
        }

        return gebruikteVerlofUren;
    }
}
