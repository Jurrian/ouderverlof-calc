package com.jurrianhartveldt.ouderverlof.lib

import com.jurrianhartveldt.ouderverlof.model.ContractPeriode
import com.jurrianhartveldt.ouderverlof.model.Kind
import com.jurrianhartveldt.ouderverlof.model.VerlofDag
import com.jurrianhartveldt.ouderverlof.model.VerlofPeriode
import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.Month
import java.time.Year

internal class VerlofCalculatorTest {

    @Nested
    inner class `getTotaalVerlof()` {

        @Test
        fun `Gets the total leave`() {
            val contractPeriode = createTestContractPeriode(36, "Company Z")

            val resultInHours =
                VerlofCalculator().getTotaalVerlof(contractPeriode, listOf(Kind("Zora", LocalDate.of(2020, 1, 1))))

            resultInHours `should be equal to` 936
        }

        @Test
        fun `Gets the total leave for different contract hours`() {
            val contractPeriode = createTestContractPeriode(40, "Bedrijf Q")

            val resultInHours =
                VerlofCalculator().getTotaalVerlof(contractPeriode, listOf(Kind("Quentin", LocalDate.of(2020, 1, 1))))

            resultInHours `should be equal to` 1040
        }

        @Test
        fun `Gets the total leave with no kids provided`() {
            val contractPeriode = createTestContractPeriode(36, "Bedrijf X")

            val resultInHours = VerlofCalculator().getTotaalVerlof(contractPeriode, listOf())

            resultInHours `should be equal to` 936
        }

        @Test
        fun `Gets the total leave for three kids`() {
            val contractPeriode = createTestContractPeriode(36, "Bedrijf Z")

            val kinderen = listOf(
                Kind("Arnold", LocalDate.of(2015, 1, 2)),
                Kind("Berend", LocalDate.of(2017, 2, 4)),
                Kind("Chris", LocalDate.of(2020, 3, 6))
            )

            val resultInHours = VerlofCalculator().getTotaalVerlof(contractPeriode, kinderen)

            resultInHours `should be equal to` 2808
        }
    }

    @Nested
    inner class `getGebruiktVerlof()` {

        @Test
        fun `Calculates the leave-hours used for one leave period`() {
            val contractPeriode = createTestContractPeriode(36, "Company D limited")
            val kind = Kind("Dominique", LocalDate.of(2020, 12, 12))
            val vrijdagMiddag = VerlofDag(DayOfWeek.FRIDAY, 4)
            val verlofPeriode = createVerlofPeriode(kind, vrijdagMiddag, Month.JANUARY)

            val hoursSpent = VerlofCalculator().getGebruiktVerlof(listOf(contractPeriode), listOf(verlofPeriode))

            // NOTE: January 1st is a national holiday. However, also national holidays count for taking leave hours of the total
            hoursSpent `should be equal to` 20
        }

        @Test
        fun `Calculates the total leave-hours used for two leave periods`() {
            val contractPeriode = createTestContractPeriode(36, "Company E inc.")
            val kind = Kind("Eveline", LocalDate.of(2020, 12, 12))
            val vrijdagMiddag = VerlofDag(DayOfWeek.FRIDAY, 4)
            val verlofPeriode1 = createVerlofPeriode(kind, vrijdagMiddag, Month.JANUARY)
            val verlofPeriode2 = createVerlofPeriode(kind, vrijdagMiddag, Month.FEBRUARY, Month.MAY)

            val hoursSpent =
                VerlofCalculator().getGebruiktVerlof(listOf(contractPeriode), listOf(verlofPeriode1, verlofPeriode2))

            hoursSpent `should be equal to` 88
        }
    }

    @Nested
    inner class `getGebruiktVerlofOpMoment()` {
        @Test
        fun `Calculates the leave-hours up to halfway the month`() {
            val contractPeriode = createTestContractPeriode(36, "Company F corp")
            val kind = Kind("Frank", LocalDate.of(2020, 12, 12))
            val wednesdayAfternoon = VerlofDag(DayOfWeek.WEDNESDAY, 4)
            val verlofPeriode = createVerlofPeriode(kind, wednesdayAfternoon, Month.JANUARY)
            val meetMoment = LocalDate.of(2021, Month.JANUARY, 15)

            val hoursSpent =
                VerlofCalculator().getGebruiktVerlofOpMoment(listOf(contractPeriode), listOf(verlofPeriode), meetMoment)

            hoursSpent `should be equal to` 8
        }

        @Test
        fun `Doesn't calculate leave-hours for periodes not yet started`() {
            val contractPeriode = createTestContractPeriode(36, "Company F corp")
            val kind = Kind("Frank", LocalDate.of(2020, 12, 12))
            val wednesdayAfternoon = VerlofDag(DayOfWeek.WEDNESDAY, 4)

            val verlofPeriode = createVerlofPeriode(kind, wednesdayAfternoon, Month.MAY)
            val meetMoment = LocalDate.of(2021, Month.FEBRUARY, 1)

            val hoursSpent =
                VerlofCalculator().getGebruiktVerlofOpMoment(listOf(contractPeriode), listOf(verlofPeriode), meetMoment)

            hoursSpent `should be equal to` 0
        }

        @Test
        fun `Calculates leave-hours for several leave periods fully`() {
            val contractPeriode = createTestContractPeriode(36, "Company F corp")
            val kind = Kind("Frank", LocalDate.of(2020, 12, 12))
            val wednesdayAfternoon = VerlofDag(DayOfWeek.WEDNESDAY, 4)

            val verlofPeriode1 = createVerlofPeriode(kind, wednesdayAfternoon, Month.FEBRUARY)
            val verlofPeriode2 = createVerlofPeriode(kind, wednesdayAfternoon, Month.APRIL, Month.MAY)
            val meetMoment = LocalDate.of(2021, Month.JUNE, 1)

            val hoursSpent = VerlofCalculator().getGebruiktVerlofOpMoment(
                listOf(contractPeriode),
                listOf(verlofPeriode1, verlofPeriode2),
                meetMoment
            )

            hoursSpent `should be equal to` 48
        }
    }

    private fun createVerlofPeriode(
        kind: Kind,
        vrijdagMiddag: VerlofDag,
        startMaand: Month,
        eindMaand: Month = startMaand
    ) =
        VerlofPeriode(
            kind,
            LocalDate.of(2021, startMaand, 1),
            LocalDate.of(2021, eindMaand, eindMaand.length(Year.isLeap(2021))),
            setOf(vrijdagMiddag)
        )

    private fun createTestContractPeriode(
        weekUren: Int,
        bedrijfsNaam: String
    ): ContractPeriode =
        ContractPeriode(LocalDate.of(2020, 1, 1), LocalDate.of(2022, 1, 1), weekUren, bedrijfsNaam)
}
