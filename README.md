# Ouderschapsverlof Calculator [![pipeline status](https://gitlab.com/Jurrian/ouderverlof-calc/badges/master/pipeline.svg)](https://gitlab.com/Jurrian/ouderverlof-calc/-/commits/master) [![coverage report](https://gitlab.com/Jurrian/ouderverlof-calc/badges/master/coverage.svg)](https://gitlab.com/Jurrian/ouderverlof-calc/-/commits/master)

Een ouderschapsverlof calculator om ouderschapsverlof mee te plannen volgens regels van de Nederlandse overheid bij Nederlandse bedrijven.

## Hoe werk het?

Het is mogelijk gegevens in te vullen zoals kinderen, contracturen en verschillende verlofperiodes om vervolgens te zien hoeveel ouderschapsverlofdagen een verlofperiode opgebruikt heeft en hoeveel er over is.

## Roadmap

### Wat er is
- Model
- Een library die de methodes aanrijkt om berekeningen te doen

### Wat ik plan te doen
- Een service die het mogelijk maakt gegevens in te voeren en rapportages te genereren (in het geheugen)
- Een interface die de service ondesteund voor menselijke interactie
